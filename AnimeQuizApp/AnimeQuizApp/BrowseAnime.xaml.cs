﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AnimeQuizApp.Models;
using ModernHttpClient;
using System.Net.Http;
using SF3.Models3;

using System.Collections.ObjectModel;

namespace AnimeQuizApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BrowseAnime : ContentPage
	{
        ObservableCollection<AnimeByIdClass> animeCollection = new ObservableCollection<AnimeByIdClass>();
        int count = 0;
        public BrowseAnime ()
		{
			InitializeComponent ();
            this.BindingContext = this;
            IsBusy = true;
            AnimeListView.ItemsSource = animeCollection;
            PopulateListView();
           
            
        }
        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            PopulateListView();
            AnimeListView.IsRefreshing = false;
        }
        void Handle_ContextMenuMoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var animeObject = (AnimeByIdClass)menuItem.CommandParameter;
            Navigation.PushAsync(new DisplayPage1(animeObject));
        }
        void Handle_ContextDelete(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var animeObject = (AnimeByIdClass)menuItem.CommandParameter;
            animeCollection.Remove(animeObject);
        }
        async void PopulateListView()
        {
                Random rnd = new Random();
                bool success = false;            
            do
                {
                    var client = new HttpClient(new NativeMessageHandler());

                    int randNum = rnd.Next(1, 3516);
                    String randS = randNum.ToString();
                    var uri = new Uri(
                        string.Format($"https://api.jikan.moe/anime/" + randS));
                    var request = new HttpRequestMessage();
                    request.Method = HttpMethod.Get;
                    request.RequestUri = uri;
                    request.Headers.Add("Application", "application / json");

                    //SearchAnimeNameList searchAnimeNameListObject;
                    HttpResponseMessage response; response = await client.SendAsync(request);
                    AnimeByIdClass animeObject;
                if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        //searchAnimeName= SearchAnimeNameList.FromJson(content);
                        animeObject = AnimeByIdClass.FromJson(content);
                        if (animeObject.TitleEnglish != null)
                        {
                        animeObject.TitleEnglish = System.Web.HttpUtility.HtmlDecode(animeObject.TitleEnglish);

                        }
                    else
                    {
                        animeObject.TitleEnglish = "English title not found";
                    }

                    if (animeObject.TitleEnglish.Equals("English title not found"))
                    {
                        if (animeObject.TitleJapanese.Equals("Japanese title not found"))
                        {
                            animeObject.Title = "No Title was found";
                        }
                        else
                        {
                            animeObject.Title = animeObject.TitleJapanese;
                        }
                    }
                    else
                    {
                        animeObject.Title = animeObject.TitleEnglish;
                    }
                        if (animeObject.Synopsis != null)
                        {
                        animeObject.Synopsis = System.Web.HttpUtility.HtmlDecode(animeObject.Synopsis);
                    }
                        else
                        {
                            animeObject.Synopsis = "Synopsis not found";
                        }
                    if (animeObject != null && animeObject.Rating != null && !animeObject.Rating.Contains("R")) 
                        animeCollection.Add(animeObject);
                    if (animeCollection.Count >= 5)
                    {
                        IsBusy = false;
                    }
                    count++;
                    }
                } while (!success);
            }

		async void GoToSearchPage(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new SearchPage());
		}
    }
    
}