﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimeQuizApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnimeQuizApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DisplayPage1 : ContentPage
	{
        AnimeByIdClass animeObject;
		public DisplayPage1 (AnimeByIdClass animeObject)
		{
            this.animeObject = animeObject;
			InitializeComponent ();
            if(animeObject.TitleEnglish.Equals("English title not found"))
            {
                if(animeObject.TitleJapanese.Equals("Japanese title not found"))
                {
                    DisplayTitle.Text = "No Title was found";
                }
                else
                {
                    DisplayTitle.Text = animeObject.TitleJapanese;
                }
            }
            else
            {
                DisplayTitle.Text = animeObject.TitleEnglish;
            }
            Uri uri = new Uri(animeObject.ImageUrl);
            AnimeImage.Source = ImageSource.FromUri(uri);
            DisplaySynopsis.Text = animeObject.Synopsis;
		}
         void GoToWebsiteButtonClicked(Object sender, System.EventArgs e)
        {
            String uriString = "https://myanimelist.net/anime/" + animeObject.MalId;
            var uri = new Uri(uriString);
            Device.OpenUri(uri);
        }
    }
}