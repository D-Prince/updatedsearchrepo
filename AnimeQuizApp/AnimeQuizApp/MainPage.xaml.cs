﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using ModernHttpClient;
using Plugin.Connectivity;
using AnimeQuizApp.Models;
using Xamarin.Forms;
using Newtonsoft.Json;

namespace AnimeQuizApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            UpdateConnectionLabel();
            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                UpdateConnectionLabel();
            };
            this.BindingContext = this;
            //IsBusy = true;
            //SetBackgroundImage();
            //BackgroundImage = "interface1.png";
            
        }
        private void UpdateConnectionLabel()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    DisplayAlert("Alert", "You are not connected to internet", "OK");
                });
            }
        }
        async void ListViewDisplayButtonClicked(Object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new BrowseAnime());
          
    }

        async void QueryButtonClicked(Object sender, System.EventArgs e)
        {
            Random rnd = new Random();
            bool success = false;
            do
            {
                var client = new HttpClient(new NativeMessageHandler());
                
                int randNum = rnd.Next(1, 3516);
                String randS = randNum.ToString();
                var uri = new Uri(
                    string.Format($"https://api.jikan.moe/anime/" + randS));
                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                AnimeByIdClass animeObject;
                //SearchAnimeNameList searchAnimeNameListObject;
                HttpResponseMessage response; response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    //searchAnimeName= SearchAnimeNameList.FromJson(content);
                    animeObject = AnimeByIdClass.FromJson(content);
                    if (animeObject.TitleEnglish != null)
                    {
                        animeObject.TitleEnglish = System.Web.HttpUtility.HtmlDecode(animeObject.TitleEnglish);
                    }
                    else
                    {
                        animeObject.TitleEnglish = "English title not found";
                    }
                    if (animeObject.TitleJapanese != null)
                    {
                        String japanese = System.Uri.UnescapeDataString(animeObject.TitleJapanese);
                        animeObject.TitleJapanese = japanese;
                    }
                    else
                    {
                        animeObject.TitleJapanese = "Japanese title not found";
                    }
                    if (animeObject.Synopsis != null)
                    {
                        animeObject.Synopsis = System.Web.HttpUtility.HtmlDecode(animeObject.Synopsis);
                    }
                    else
                    {
                        animeObject.Synopsis = "Synopsis not found";
                    }
                    if(animeObject.Rating != null && !animeObject.Rating.Contains("R"))
                    {
                        await Navigation.PushAsync(new DisplayPage1(animeObject));
                        success = true;
                    }
                    
                }
            }while(!success);
        }
        async void QuizButtonClicked(Object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new QuizPage3());

        }
        async void SetBackgroundImage()
        {
            Random rnd = new Random();
            bool success = false;
            do
            {
                var client = new HttpClient(new NativeMessageHandler());

                int randNum = rnd.Next(1, 3516);
               String randS = randNum.ToString();
                var uri = new Uri(
                    string.Format($"https://api.jikan.moe/anime/" + randS));
                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                AnimeByIdClass animeObject;
                //SearchAnimeNameList searchAnimeNameListObject;
                HttpResponseMessage response; response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    //searchAnimeName= SearchAnimeNameList.FromJson(content);
                    animeObject = AnimeByIdClass.FromJson(content);
                    if (animeObject.TitleEnglish != null)
                    {
                        animeObject.TitleEnglish = System.Web.HttpUtility.HtmlDecode(animeObject.TitleEnglish);
                    }
                    else
                    {
                        animeObject.TitleEnglish = "English title not found";
                    }
                    if (animeObject.TitleJapanese != null)
                    {
                        String japanese = System.Uri.UnescapeDataString(animeObject.TitleJapanese);
                        animeObject.TitleJapanese = japanese;
                    }
                    else
                    {
                        animeObject.TitleJapanese = "Japanese title not found";
                    }
                    if (animeObject.Synopsis != null)
                    {
                        animeObject.Synopsis = System.Web.HttpUtility.HtmlDecode(animeObject.Synopsis);
                    }
                    else
                    {
                        animeObject.Synopsis = "Synopsis not found";
                    }
                    if (animeObject.Rating != null && !animeObject.Rating.Contains("R"))
                    {
                        //BackgroundImageView.Source = animeObject.ImageUrl;
                        success = true;
                    }

                }
            } while (!success);
            IsBusy = false;
        }
    }
}
