﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using AnimeQuizApp.Models;
//
//    var animeByIdClass = AnimeByIdClass.FromJson(jsonString);

namespace AnimeQuizApp.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class AnimeByIdClass
    {
        [JsonProperty("request_hash")]
        public string RequestHash { get; set; }

        [JsonProperty("request_cached")]
        public bool RequestCached { get; set; }

        [JsonProperty("mal_id")]
        public long? MalId { get; set; }

        [JsonProperty("link_canonical")]
        public string LinkCanonical { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("title_english")]
        public string TitleEnglish { get; set; }

        [JsonProperty("title_japanese")]
        public string TitleJapanese { get; set; }

        [JsonProperty("title_synonyms")]
        public object TitleSynonyms { get; set; }

        [JsonProperty("image_url")]
        public string ImageUrl { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("episodes")]
        public long? Episodes { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("airing")]
        public bool Airing { get; set; }

        [JsonProperty("aired_string")]
        public string AiredString { get; set; }

        [JsonProperty("aired")]
        public Aired Aired { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("rating")]
        public string Rating { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }

        [JsonProperty("scored_by")]
        public long? ScoredBy { get; set; }

        [JsonProperty("rank")]
        public long? Rank { get; set; }

        [JsonProperty("popularity")]
        public long? Popularity { get; set; }

        [JsonProperty("members")]
        public long Members { get; set; }

        [JsonProperty("favorites")]
        public long? Favorites { get; set; }

        [JsonProperty("synopsis")]
        public string Synopsis { get; set; }

        [JsonProperty("background")]
        public object Background { get; set; }

        [JsonProperty("premiered")]
        public string Premiered { get; set; }

        [JsonProperty("broadcast")]
        public string Broadcast { get; set; }

       /* [JsonProperty("related")]
        public Related Related { get; set; }*/

        [JsonProperty("producer")]
        public Genre[] Producer { get; set; }

        [JsonProperty("licensor")]
        public Genre[] Licensor { get; set; }

        [JsonProperty("studio")]
        public Genre[] Studio { get; set; }

        [JsonProperty("genre")]
        public Genre[] Genre { get; set; }

        [JsonProperty("opening_theme")]
        public string[] OpeningTheme { get; set; }

        [JsonProperty("ending_theme")]
        public string[] EndingTheme { get; set; }
    }

    public partial class Aired
    {
        [JsonProperty("from")]
        public DateTimeOffset? From { get; set; }

        [JsonProperty("to")]
        public DateTimeOffset? To { get; set; }
    }

    public partial class Genre
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class Related
    {
        [JsonProperty("Adaptation")]
        public Adaptation[] Adaptation { get; set; }
    }

    public partial class Adaptation
    {
        [JsonProperty("mal_id")]
        public long? MalId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }

    public partial class AnimeByIdClass
    {
        public static AnimeByIdClass FromJson(string json) => JsonConvert.DeserializeObject<AnimeByIdClass>(json, AnimeQuizApp.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this AnimeByIdClass self) => JsonConvert.SerializeObject(self, AnimeQuizApp.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
