﻿using System;
namespace SF3.Models3
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class SearchFunction2
    {
        [JsonProperty("request_hash")]
        public string RequestHash { get; set; }

        [JsonProperty("request_cached")]
        public bool RequestCached { get; set; }

        [JsonProperty("result")]
        public Result[] Result { get; set; }

        [JsonProperty("result_last_page")]
        public long ResultLastPage { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("mal_id")]
        public long MalId { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("image_url")]
        public string ImageUrl { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("type")]
        public TypeEnum Type { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }

        [JsonProperty("episodes")]
        public long Episodes { get; set; }

        [JsonProperty("members")]
        public long Members { get; set; }
    }

    public enum TypeEnum { Movie, Music, Ona, Ova, Special, Tv };

    public partial class SearchFunction2
    {
        public static SearchFunction2 FromJson(string json) => JsonConvert.DeserializeObject<SearchFunction2>(json, SF3.Models3.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this SearchFunction2 self) => JsonConvert.SerializeObject(self, SF3.Models3.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new TypeEnumConverter(),
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class TypeEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(TypeEnum) || t == typeof(TypeEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "Movie":
                    return TypeEnum.Movie;
                case "Music":
                    return TypeEnum.Music;
                case "ONA":
                    return TypeEnum.Ona;
                case "OVA":
                    return TypeEnum.Ova;
                case "Special":
                    return TypeEnum.Special;
                case "TV":
                    return TypeEnum.Tv;
            }
            throw new Exception("Cannot unmarshal type TypeEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (TypeEnum)untypedValue;
            switch (value)
            {
                case TypeEnum.Movie:
                    serializer.Serialize(writer, "Movie"); return;
                case TypeEnum.Music:
                    serializer.Serialize(writer, "Music"); return;
                case TypeEnum.Ona:
                    serializer.Serialize(writer, "ONA"); return;
                case TypeEnum.Ova:
                    serializer.Serialize(writer, "OVA"); return;
                case TypeEnum.Special:
                    serializer.Serialize(writer, "Special"); return;
                case TypeEnum.Tv:
                    serializer.Serialize(writer, "TV"); return;
            }
            throw new Exception("Cannot marshal type TypeEnum");
        }
    }
}