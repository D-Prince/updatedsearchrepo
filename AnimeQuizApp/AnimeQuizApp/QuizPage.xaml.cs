﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using ModernHttpClient;
using Plugin.Connectivity;
using AnimeQuizApp.Models;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Xamarin.Forms.Xaml;

namespace AnimeQuizApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuizPage : ContentPage
    {
        bool answerSelected=false;
        int DIFFICULTY_LEVEL = 4;
        int NUM_QUESTIONS = 10;
        int NUM_QUESTION_TYPES = 3;
        string finalCorrectAnswer;
        AnimeByIdClass answerAnime;
        ObservableCollection<string> answers = new ObservableCollection<string>();
        ObservableCollection<string> wrongAnswers = new ObservableCollection<string>();
        public QuizPage()
        {
            InitializeComponent();
            this.BindingContext = this;
            IsBusy = true;
            ParseJSON();
        }
        void AddAnswer(string answer)
        {
            answers.Add(answer);
        }

        void PickerItemSelected(Object sender, System.EventArgs e)
        {

            if (answerSelected == false && answers.Count==DIFFICULTY_LEVEL)
            {

                if (AnswerPicker.SelectedItem.Equals(finalCorrectAnswer))
                {
                    AnswerDisplay.Text = "Congratulations you answered correctly!";
                    AnswerDisplay.TextColor = Color.Green;
                }
                else
                {
                    AnswerDisplay.Text = "Sorry you answered incorrectly!";
                    AnswerDisplay.TextColor = Color.Red;
                }
                answerSelected = true;
            }
        }
        async void ParseJSON()
        {

            Random rnd = new Random();
            bool success = false;
            string correctAnswer="";
            do
            {
                var client = new HttpClient(new NativeMessageHandler());

                int randNum = rnd.Next(1, 3516);
                String randS = randNum.ToString();
                var uri = new Uri(
                    string.Format($"https://api.jikan.moe/anime/" + randS));
                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                AnimeByIdClass animeObject;
                //SearchAnimeNameList searchAnimeNameListObject;
                HttpResponseMessage response; response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    //searchAnimeName= SearchAnimeNameList.FromJson(content);
                    animeObject = AnimeByIdClass.FromJson(content);
                    if (animeObject.TitleEnglish != null)
                    {
                        animeObject.TitleEnglish = System.Web.HttpUtility.HtmlDecode(animeObject.TitleEnglish);
                    }
                    else
                    {
                        animeObject.TitleEnglish = "English title not found";
                    }
                    if (animeObject.TitleJapanese != null)
                    {
                        String japanese = System.Uri.UnescapeDataString(animeObject.TitleJapanese);
                        animeObject.TitleJapanese = japanese;
                    }
                    else
                    {
                        animeObject.TitleJapanese = "Japanese title not found";
                    }
                    if (animeObject.Synopsis != null)
                    {
                        animeObject.Synopsis = System.Web.HttpUtility.HtmlDecode(animeObject.Synopsis);
                    }
                    else
                    {
                        animeObject.Synopsis = "Synopsis not found";
                    }
                    if (animeObject.Rating != null && !animeObject.Rating.Contains("R"))
                    {
                        QuestionLabel.Text = "What Anime is being described here?";
                        InfoLabel.Text = animeObject.Synopsis;
                        correctAnswer = System.Web.HttpUtility.HtmlDecode(animeObject.Title);
                        answerAnime = animeObject;
                        success = true;
                    }
                }
            } while (!success);
            
            finalCorrectAnswer = correctAnswer;
            GetWrongAnswers();
        }
        async void GetWrongAnswers()
        {
            for (int j = 0; j < DIFFICULTY_LEVEL - 1; j++)
            {

                string wrongAnswer = "cat";
                Random rnd = new Random();
                bool success = false;
                do
                {
                    var client = new HttpClient(new NativeMessageHandler());

                    int randNum = rnd.Next(1, 3516);
                    String randS = randNum.ToString();
                    var uri = new Uri(
                        string.Format($"https://api.jikan.moe/anime/" + randS));
                    var request = new HttpRequestMessage();
                    request.Method = HttpMethod.Get;
                    request.RequestUri = uri;
                    request.Headers.Add("Application", "application / json");

                    AnimeByIdClass animeObject;
                    //SearchAnimeNameList searchAnimeNameListObject;
                    HttpResponseMessage response; response = await client.SendAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        //searchAnimeName= SearchAnimeNameList.FromJson(content);
                        animeObject = AnimeByIdClass.FromJson(content);
                        if (animeObject.TitleEnglish != null)
                        {
                            animeObject.TitleEnglish = System.Web.HttpUtility.HtmlDecode(animeObject.TitleEnglish);
                        }
                        else
                        {
                            animeObject.TitleEnglish = "English title not found";
                        }
                        if (animeObject.TitleJapanese != null)
                        {
                            String japanese = System.Uri.UnescapeDataString(animeObject.TitleJapanese);
                            animeObject.TitleJapanese = japanese;
                        }
                        else
                        {
                            animeObject.TitleJapanese = "Japanese title not found";
                        }
                        if (animeObject.Synopsis != null)
                        {
                            animeObject.Synopsis = System.Web.HttpUtility.HtmlDecode(animeObject.Synopsis);
                        }
                        else
                        {
                            animeObject.Synopsis = "Synopsis not found";
                        }
                        if (animeObject.Rating != null && !animeObject.Rating.Contains("R"))
                        {
                            if (!animeObject.Title.ToString().Equals(finalCorrectAnswer))
                            {
                                wrongAnswer = System.Web.HttpUtility.HtmlDecode(animeObject.Title);
                                answers.Add(wrongAnswer);
                                success = true;
                            }
                        }

                    }
                } while (!success);

            }
            Random rnd2 = new Random();
            int randNum2 = rnd2.Next(0, DIFFICULTY_LEVEL+1);
            if (randNum2 < DIFFICULTY_LEVEL)
                answers.Insert(randNum2, finalCorrectAnswer);
            else
                answers.Add(finalCorrectAnswer);
            AnswerPicker.ItemsSource = answers;
            IsBusy = false;
        }
        void LearnMoreButtonClicked(Object sender, System.EventArgs e)
        {
            if (answerSelected == true)
            {
                Navigation.PushAsync(new DisplayPage1(answerAnime));
            }
        }
        void NextQuestionButtonClicked(Object sender, System.EventArgs e)
        {
            if (AnswerDisplay.Text != null && AnswerDisplay.Text != "")
            {
                Random random = new Random();
                int rand = random.Next(1, NUM_QUESTION_TYPES+1);
                switch (rand)
                {
                    case 1://same type question
                        if (answers.Count > 0)
                        {
                            answers.Clear();
                        }
                        if (wrongAnswers.Count > 0)
                        {
                            wrongAnswers.Clear();
                        }
                        answerSelected = false;
                        AnswerDisplay.Text = "";
                        IsBusy = true;
                        ParseJSON();

                        break;
                    case 2:
                        Navigation.PushAsync(new QuizPage2());
                        break;
                    case 3:
                        Navigation.PushAsync(new QuizPage3());
                        break;
                }

            }
        }
    }
}