﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using ModernHttpClient;
using Plugin.Connectivity;
using AnimeQuizApp.Models;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Xamarin.Forms.Xaml;

namespace AnimeQuizApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QuizPage2 : ContentPage
	{
        int NUM_QUESTIONS = 10;
        int NUM_QUESTION_TYPES = 3;
        int DIFFICULTY_LEVEL = 4;
        AnimeByIdClass answerAnime;
        bool answerSelected = false;
        ObservableCollection<AnimeByIdClass> animeCollection = new ObservableCollection<AnimeByIdClass>();
        public QuizPage2 ()
		{
			InitializeComponent();
            this.BindingContext = this;
            IsBusy = true;
            ParseJSON();
		}

        void OnTapGestureRecognizerTapped(object sender, EventArgs args)
        {
            var imageSender = (Image)sender;
            
            if (answerSelected==false)
            {
                imageSender.BackgroundColor = Color.Red;
                string uriString = "Uri: " + answerAnime.ImageUrl;
                if (uriString.Equals(imageSender.Source.ToString()))
                {
                    AnswerDisplay.Text = "Congratulations you are correct!";
                    AnswerDisplay.TextColor = Color.Green;                
                }
                else
                {
                    AnswerDisplay.Text = "Sorry you are incorrect!";
                    AnswerDisplay.TextColor = Color.Red;
                }
                answerSelected = true;
            }
        }
        async void ParseJSON()
        {
            for (int j = 0; j < DIFFICULTY_LEVEL; j++)
            {
                Random rnd = new Random();
                bool success = false;
                do
                {
                    var client = new HttpClient(new NativeMessageHandler());

                    int randNum = rnd.Next(1, 3516);
                    String randS = randNum.ToString();
                    var uri = new Uri(
                        string.Format($"https://api.jikan.moe/anime/" + randS));
                    var request = new HttpRequestMessage();
                    request.Method = HttpMethod.Get;
                    request.RequestUri = uri;
                    request.Headers.Add("Application", "application / json");

                    AnimeByIdClass animeObject;
                    
                    HttpResponseMessage response; response = await client.SendAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        
                        animeObject = AnimeByIdClass.FromJson(content);
                        if (animeObject.TitleEnglish != null)
                        {
                            animeObject.TitleEnglish = System.Web.HttpUtility.HtmlDecode(animeObject.TitleEnglish);
                        }
                        else
                        {
                            animeObject.TitleEnglish = "English title not found";
                        }
                        if (animeObject.TitleJapanese != null)
                        {
                            String japanese = System.Uri.UnescapeDataString(animeObject.TitleJapanese);
                            animeObject.TitleJapanese = japanese;
                        }
                        else
                        {
                            animeObject.TitleJapanese = "Japanese title not found";
                        }
                        if (animeObject.Synopsis != null)
                        {
                            animeObject.Synopsis = System.Web.HttpUtility.HtmlDecode(animeObject.Synopsis);
                        }
                        else
                        {
                            animeObject.Synopsis = "Synopsis not found";
                        }
                        if (animeObject.Rating != null && !animeObject.Rating.Contains("R"))
                        {
                            if (animeObject.ImageUrl!=null)
                            {
                                for(int i = 0; i < animeCollection.Count; i++)
                                {
                                    if (animeObject.ImageUrl.Equals(animeCollection[i].ImageUrl))
                                    {
                                        //already exists
                                        animeObject = null;
                                    }
                                }
                                if (animeObject!=null)
                                {
                                    animeCollection.Add(animeObject);
                                    
                                    success = true;
                                }
                               
                            }
                        }

                    }
                } while (!success);
                if (animeCollection.Count == DIFFICULTY_LEVEL)
                {
                    Random random = new Random();
                    int rand = random.Next(0, DIFFICULTY_LEVEL);
                    answerAnime = animeCollection[rand];
                    QuestionLabel.Text = "Which Anime is: " + System.Web.HttpUtility.HtmlDecode(answerAnime.Title+"?");
                    ImageView1.Source = animeCollection[0].ImageUrl;
                    ImageView2.Source = animeCollection[1].ImageUrl;
                    ImageView3.Source = animeCollection[2].ImageUrl;
                    ImageView4.Source = animeCollection[3].ImageUrl;
                    IsBusy = false;
                }
            }
        }
        void LearnMoreButtonClicked(Object sender, System.EventArgs e)
        {
            if (answerSelected == true)
            {
                Navigation.PushAsync(new DisplayPage1(answerAnime));
            }
        }
        void NextQuestionButtonClicked(Object sender, System.EventArgs e)
        {
            if (AnswerDisplay.Text != null && AnswerDisplay.Text != "")
            {
                Random random = new Random();
                int rand = random.Next(1, NUM_QUESTION_TYPES + 1);
                switch (rand)
                {
                    case 1://same type question
                        Navigation.PushAsync(new QuizPage());
                        break;
                    case 2:
                        answerAnime = null;
                        answerSelected = false;
                        if (animeCollection.Count > 0)
                        {
                            animeCollection.Clear();
                        }
                        AnswerDisplay.Text = "";
                        IsBusy = true;
                        ParseJSON();
                        break;
                    case 3:
                        Navigation.PushAsync(new QuizPage3());
                        break;
                }

            }
        }
    }
}