﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ModernHttpClient;
using System.Net.Http;
using SF3.Models3;
namespace AnimeQuizApp
{
    public partial class SearchPage : ContentPage
    {
        public SearchPage()
        {
			InitializeComponent();
        }

		async void SearchButton(object sender, System.EventArgs e)
		{
			HttpClient client = new HttpClient();

            var getWordSwag = search.Text.ToString();


            var uri = new Uri(string.Format($"https://api.jikan.moe/search/anime/" + getWordSwag + "/1"));

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");

            HttpResponseMessage response = await client.SendAsync(request);

            SearchFunction2 wordData = null;
			if (response.IsSuccessStatusCode)
			{
				var content = await response.Content.ReadAsStringAsync();
				wordData = SearchFunction2.FromJson(content);
				await Navigation.PushAsync(new SearchPassPage(wordData));
			}
		}
    }
}
