﻿using System;
using System.Collections.Generic;
//using SF3.Models2;
using SF3.Models3;

using Xamarin.Forms;

namespace AnimeQuizApp
{
    public partial class SearchPassPage : ContentPage
    {
        public SearchPassPage(SearchFunction2 wordData)
        {
            InitializeComponent();

            title.Text = wordData.Result[0].Title;

            description.Text = wordData.Result[0].Description;

            rating.Text = "Rating: " + wordData.Result[0].Score.ToString() + "/10";

            episodes.Text = "Episodes: " + wordData.Result[0].Episodes.ToString();


        }
    }
}

