﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AnimeQuizApp.Models;


namespace AnimeQuizApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WebsiteMoreInfoPage : ContentPage
	{
		public WebsiteMoreInfoPage (AnimeByIdClass animeObject)
		{
			InitializeComponent ();
		}
        void GoToWebsite(object sender, System.EventArgs e)
        {
            AnimeByIdClass itemTapped = (AnimeByIdClass)BindingContext;
            String uriString = "https://myanimelist.net/anime/" + itemTapped.MalId;
            var uri = new Uri(uriString);
            Device.OpenUri(uri);
        }
       
	}
}